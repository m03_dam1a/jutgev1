

// Problema 1
val jocDeProvesPublic1 = JocDeProves("Parlar", "6")
val jocDeProvesPrivat1 = JocDeProves("Aquest exemple.", "15")
val problema1 = Problema("1.1 Mida d’un String\n" +
        "Descripció\n" +
        "Escriu un programa que cridi una funció que imiti la propietat .length de la classe String.", jocDeProvesPublic1, jocDeProvesPrivat1)

// Problema 2
val jocDeProvesPublic2 = JocDeProves("Codi", "4")
val jocDeProvesPrivat2 = JocDeProves("Aq23", "6")
val problema2 = Problema("1.2 Comprovació de codi\n" +
        "Descripció\n" +
        "Escriu un programa que cridi una funció que comprovi la validesa d'un codi específic.", jocDeProvesPublic2, jocDeProvesPrivat2)

// Problema 3
val jocDeProvesPublic3 = JocDeProves("Text", "9")
val jocDeProvesPrivat3 = JocDeProves("Exemple de text.", "17")
val problema3 = Problema("1.3 Comptador de caràcters\n" +
        "Descripció\n" +
        "Escriu un programa que cridi una funció que compti el nombre de caràcters en un text donat.", jocDeProvesPublic3, jocDeProvesPrivat3)

// Problema 4
val jocDeProvesPublic4 = JocDeProves("Llista", "3")
val jocDeProvesPrivat4 = JocDeProves("1, 2, 3", "5")
val problema4 = Problema("1.4 Longitud de llista\n" +
        "Descripció\n" +
        "Escriu un programa que cridi una funció que calculi la longitud d'una llista donada.", jocDeProvesPublic4, jocDeProvesPrivat4)

// Problema 5
val jocDeProvesPublic5 = JocDeProves("Text", "7")
val jocDeProvesPrivat5 = JocDeProves("Exemple de text.", "12")
val problema5 = Problema("1.5 Comptador de vocals\n" +
        "Descripció\n" +
        "Escriu un programa que cridi una funció que compti el nombre de vocals en un text donat.", jocDeProvesPublic5, jocDeProvesPrivat5)

// Problema 6
val jocDeProvesPublic6 = JocDeProves("Cadena", "2")
val jocDeProvesPrivat6 = JocDeProves("Ex", "4")
val problema6 = Problema("1.6 Comprovació de cadena buida\n" +
        "Descripció\n" +
        "Escriu un programa que cridi una funció que comprovi si una cadena donada és buida.", jocDeProvesPublic6, jocDeProvesPrivat6)

// Problema 7
val jocDeProvesPublic7 = JocDeProves("Cadena", "5")
val jocDeProvesPrivat7 = JocDeProves("Exemp", "8")
val problema7 = Problema("1.7 Comprovació de cadena no buida\n" +
        "Descripció\n" +
        "Escriu un programa que cridi una funció que comprovi si una cadena donada no és buida.", jocDeProvesPublic7, jocDeProvesPrivat7)

// Problema 8
val jocDeProvesPublic8 = JocDeProves("Nombres", "3")
val jocDeProvesPrivat8 = JocDeProves("1, 2, 3", "6")
val problema8 = Problema("1.8 Suma de nombres\n" +
        "Descripció\n" +
        "Escriu un programa que cridi una funció que calculi la suma de tots els nombres en una llista donada.", jocDeProvesPublic8, jocDeProvesPrivat8)

// Problema 9
val jocDeProvesPublic9 = JocDeProves("Paraula", "4")
val jocDeProvesPrivat9 = JocDeProves("Codi", "2")
val problema9 = Problema("1.9 Comprovació de paraula\n" +
        "Descripció\n" +
        "Escriu un programa que cridi una funció que comprovi si una paraula donada és vàlida.", jocDeProvesPublic9, jocDeProvesPrivat9)

// Problema 10
val jocDeProvesPublic10 = JocDeProves("Text", "8")
val jocDeProvesPrivat10 = JocDeProves("Exemple de text.", "15")
val problema10 = Problema("1.10 Comptador de paraules\n" +
        "Descripció\n" +
        "Escriu un programa que cridi una funció que compti el nombre de paraules en un text donat.", jocDeProvesPublic10, jocDeProvesPrivat10)