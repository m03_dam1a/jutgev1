import java.util.*


val problemes = mutableListOf<Problema>()

val scanner = Scanner(System.`in`).useLocale(Locale.UK)

fun main() {

 cargarProblemes()
    mostrarProblema()



}

fun mostrarProblema() {
    // mostrar

    for (prm in problemes.indices) {
        println(
            "${problemes[prm].enunciat}\n" +
                    "${Colors.recuadro} ${problemes[prm].jocDeProvesPublic.input} ${Colors.reset} - " +
                    "${Colors.recuadro} ${problemes[prm].jocDeProvesPublic.output} ${Colors.reset}\n" +
                    "${Colors.recuadro} ${problemes[prm].jocDeProvesPrivat.input} ${Colors.reset} - " +
                    "${Colors.recuadro} [X] ${Colors.reset}\n  ${Colors.light_lila}Resposta: ${problemes[prm].jocDeProvesPrivat.output}${Colors.reset}"
        )

        do {
            var seguent = false
            do {
                var respost = false
                println("Vols resoldre'l: s/n")
                var resposta = scanner.next()
                when (resposta) {
                    "s" -> {
                        println("Inserta la teva resposta:")
                        var respostaUsuari = scanner.next()
                        if (respostaUsuari == problemes[prm].jocDeProvesPrivat.output) {
                            println("${Colors.light_cyan}Correcte ✅${Colors.reset}")

                            problemes[prm].NIntets + 1
                            problemes[prm].intents.add(respostaUsuari)
                            problemes[prm].resolt = true
                            seguent = true


                        } else {
                            println("${Colors.red}❌ Error. Proba un altre cop.${Colors.reset}")
                        }
                        respost = true
                    }
                    "n" -> {
                        respost = true
                        seguent = true
                    }
                    else -> {
                        println("Introdueix ${Colors.light_cyan}s${Colors.reset} si vols o ${Colors.red}n${Colors.reset} si no en vols resoldre el problema.")
                    }
                }
            }while (respost==false)


        } while (!seguent)

    }



}

fun cargarProblemes() {
    problemes.addAll(
        listOf(
            problema1,
            problema2,
            problema3,
            problema4,
            problema5,
            problema6,
            problema7,
            problema8,
            problema9,
            problema10
        )
    )
}

